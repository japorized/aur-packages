**WARNING**: Always, ALWAYS refer to the [official Arch Wiki article](https://wiki.archlinux.org/index.php/AUR_submission_guidelines) for an official and updated submission process. This is simply a note to myself for quick reference instead of having to stare at more than 200 characters.

# AUR Submission Process

---

## Creating package from scratch

1. Check if package name exists by running `git clone ssh://aur@aur.archlinux.org/pkgbase.git` where `pkgbase` is *usually* `pkgname`.
  * Package exists if cloned repo is non-empty
    * Best not to override package - seek another name
  * Package DNE if cloned repo is empty
    * Proceed
2. `git init`
3. `git remote add origin `ssh://aur@aur.archlinux.org/pkgbase.git`
4. `git fetch` to initialize
5. `git push --set-upstream origin master` to push stuff up
